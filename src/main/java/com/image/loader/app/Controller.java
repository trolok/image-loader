package com.image.loader.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController("load/image")
public class Controller {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public void getImage(@RequestParam String url, @RequestParam String fileName, HttpServletResponse response) throws IOException {
        ResponseEntity<byte[]> imageBytes = restTemplate.getForEntity(url, byte[].class);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        String[] urlParts = url.split("/");
        String imageName = urlParts[urlParts.length - 1];
        ZipEntry e = new ZipEntry(imageName);
        out.putNextEntry(e);

        response.setHeader("Content-Disposition", String.format("attachment; filename=%s.zip", fileName));
        response.setHeader("Content-Type", "application/zip");

        out.write(imageBytes.getBody(), 0, imageBytes.getBody().length);
        out.closeEntry();
        out.close();
    }
}
